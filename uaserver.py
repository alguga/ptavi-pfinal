#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import simplertp
import socketserver
import sys
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import time
import random

con_port = 0
con_IP = ""


class EchoHandler(socketserver.DatagramRequestHandler):
    """ Gestiona la llegada de paquetes del destinatario"""

    def handle(self):

        linea = ""
        while 1:
            """ Bucle que lee los mensajes que recibe del cliente"""
            line = self.rfile.read()
            try:
                linea = linea + line.decode('utf-8')
            except ValueError:
                linea = linea + ""

            # Si no hay más líneas salimos del bucle infinito
            if not line:
                break
        print("Se ha recibido: " + linea[:linea.index("\r\n")])
        message = []
        global con_port
        global con_IP
        message.append(linea[0: linea.index(" ")])

        log_algorithm("Received from " + self.client_address[0] + ":"
                      + str(self.client_address[1]) + ": " + linea)

        if message[0] == "INVITE":

            """Como debe actuar el programa si recibe un invite"""

            con_port = linea[linea.index("m=") + 2: linea.index("RTP")]
            con_port = int(con_port.split(" ")[1])
            con_IP = linea[linea.index("o=") + 2: linea.index("s=") - 2]
            con_IP = con_IP.split(" ")[1]

            cuerpo = "v=0" + "\r\n"
            cuerpo = cuerpo + "o=" + user_name + " " + IP + "\r\n"
            cuerpo = cuerpo + "s=misesion" + "\r\n"
            cuerpo = cuerpo + "t=0" + "\r\n"
            cuerpo = cuerpo + "m=audio " + str(rtp_puerto) + " RTP"

            cabecera = "SIP/2.0 100 Trying  SIP/2.0 180 Ringing  " \
                       "SIP/2.0 200 OK\r\n"
            cabecera = cabecera + "Content-Type: application/sdp" + "\r\n"
            cabecera = cabecera + "Content-Length: " + str(
                len(cuerpo) + 2) + "\r\n\r\n"

            LINE = cabecera + cuerpo
            self.wfile.write(str.encode(LINE))
            print("Enviando" + LINE[:LINE.index("\r\n")])
            log_algorithm("Sent to " + self.client_address[0] + ":"
                          + str(self.client_address[1]) + ": " + LINE)

        elif message[0] == "ACK":

            """Como debe actuar el programa si recibe un ACK"""
            csrc = []
            for i in range(0, random.randint(3, 6)):
                csrc.append(random.randint(1, 9999))
            BIT = random.randint(0, 1)
            RTP_header = simplertp.RtpHeader()
            RTP_header.set_header(version=2, marker=BIT,
                                  payload_type=14, ssrc=200002, cc=len(csrc))
            RTP_header.setCSRC(csrc)
            audio = simplertp.RtpPayloadMp3(FILE)
            print("enviando: RTP")
            log_algorithm("Sent to " + con_IP + ":" + str(con_port)
                          + ":" + FILE + "via RTP")
            simplertp.send_rtp_packet(RTP_header, audio, con_IP, con_port)
            print("RTP ENVIADO")

        elif message[0] == "BYE":
            """Como debe actuar el programa si recibe un BYE"""
            print("enviando: SIP/2.0 200 OK")
            self.wfile.write(b"SIP/2.0 200 OK\r\n")
            log_algorithm(
                "Sent to " + self.client_address[0]
                + ":" + str(self.client_address[1])
                + ": " + "SIP/2.0 200 OK")

        elif message[0] != " INVITE" or "ACK" or "BYE" or "REGISTER" \
                and len(message) == 2:
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n")
            print("Enviado: SIP/2.0 405 Method Not Allowed")
            log_algorithm(" Sent to " + self.client_address[0]
                          + ":" + str(
                self.client_address[1]) + ": "
                          + "SIP/2.0 405 Method Not Allowed")

        elif len(message) != 2:
            self.wfile.write(b"SIP/2.0 400 Bad Request\r\n")
            print("Enviado: SIP/2.0 400 Bad Request")
            log_algorithm(" Sent to " + self.client_address[0]
                          + ":" + str(self.client_address[1])
                          + ": " + "SIP/2.0 400 Bad Request")

        else:
            self.wfile.write(b"SIP/2.0 400 Bad Request\r\n")
            print("Enviado: SIP/2.0 400 Bad Request")
            log_algorithm("Sent to " + self.client_address[0] + ":"
                          + str(self.client_address[1])
                          + ": " + "SIP/2.0 400 Bad Request")


class XMLHandler(ContentHandler):

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """

        self.ua_ip = ""
        self.ua_puerto = ""
        self.proxy_ip = ""
        self.proxy_puerto = ""
        self.log_path = ""
        self.rtp_puerto = ""
        self.audio = ""
        self.username = ""

    def startElement(self, name, attrs):

        if name == 'uaserver':

            self.ua_ip = attrs.get("ip")
            self.ua_puerto = attrs.get("puerto")

        elif name == 'regproxy':
            self.proxy_ip = attrs.get("ip")
            self.proxy_puerto = attrs.get("puerto")

        elif name == 'log':
            self.log_path = attrs.get("path")

        elif name == "account":
            self.username = attrs.get("username")

        elif name == "rtpaudio":
            self.rtp_puerto = attrs.get("puerto")

        elif name == "audio":
            self.audio = attrs.get("cancion.mp3")


def log_algorithm(message):
    log_file = open(Log_path, "a")
    actual_time = \
        time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time()))\
            .split(" ")
    actual_time[1] = actual_time[1].split(":")
    hora = int(actual_time[1][0]) * 3600
    mins = int(actual_time[1][1]) * 60
    seg = int(actual_time[1][2]) + hora + mins
    actual_time[1] = str(seg)
    log_info = ""
    for i in message:
        if i == "\r" or i == "\n":
            log_info = log_info + " "
        else:
            log_info = log_info + i
    log_file.write(actual_time[0] + " "
                   + actual_time[1] + " " + log_info + "\r\n")
    log_file.close()


if __name__ == "__main__":

    try:
        """Recoge la informacion con la que se conectara al cliente"""
        FILE = sys.argv[1]
        parser = make_parser()
        cHandler = XMLHandler()
        parser.setContentHandler(cHandler)
        parser.parse(open(FILE))
        IP = cHandler.ua_ip
        PORT = int(cHandler.ua_puerto)
        user_name = cHandler.username
        direction = [IP, PORT]
        rtp_puerto = int(cHandler.rtp_puerto)
        Log_path = cHandler.log_path

        file = open(Log_path, "a")
        file.close()

        if direction[0] == "":
            IP = "localhost"
            direction[0] = "localhost"
        serv = socketserver.\
            UDPServer((direction[0], direction[1]), EchoHandler)
        print("Listening...")
        log_algorithm("Starting server...")
        serv.serve_forever()

    except (ValueError, IndexError, FileNotFoundError):
        print("Usage: python uaserver.py config")
