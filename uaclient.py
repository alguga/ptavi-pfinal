"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import simplertp
import time
import random
import hashlib

IP = ""
PORT = ""
proxy = []


def message_sender():
    method_list = ["REGISTER", "INVITE", "ACK", "BYE"]
    try:

        if len(sys.argv) == 4:
            method = sys.argv[2]
            method_exist = False
            for i in method_list:
                if i == method:
                    method_exist = True
                    break
            if not method_exist:
                raise ValueError

            option = sys.argv[3]

        else:
            raise ValueError

        if method == "INVITE":

            """
                Arma el paquete INVITE para iniciar
                la conversacion correctamente
            """

            Cuerpo = "v=0" + "\r\n"
            Cuerpo = Cuerpo + "o=" + user_name + " " + IP + "\r\n"
            Cuerpo = Cuerpo + "s=misesion" + "\r\n"
            Cuerpo = Cuerpo + "t=0" + "\r\n"
            Cuerpo = Cuerpo + "m=audio " + rtp_puerto + " RTP"

            cabecera = method + " sip:" + sys.argv[3] + " SIP/2.0" + "\r\n"
            Cabecera = cabecera + "Content-Type: application/sdp" + "\r\n"
            Cabecera = Cabecera + "Content-Length: " + str(
                len(Cuerpo) + 2) + "\r\n\r\n"

            LINE = Cabecera + Cuerpo
            print("enviando:")
            print(LINE[:LINE.index("\r\n")])
            log_algorithm("Sent to " + proxy[0] + ":" + proxy[1] +
                          ":" + LINE)
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM)\
                    as my_socket:
                my_socket.setsockopt(socket.SOL_SOCKET,
                                     socket.SO_REUSEADDR, 1)
                my_socket.connect((proxy[0], int(proxy[1])))

                my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
                data = my_socket.recv(1024)
                recived = data.decode('utf-8')
                print('Recibido -- ' + recived[:recived.index("\r\n")])
                log_algorithm("Received from " + proxy[0] + ":" + proxy[1]
                              + ": " + recived)
                recived = data.decode('utf-8')

                if recived[: recived.index("\r\n")] \
                        == "SIP/2.0 100 Trying  SIP/2.0 180 " \
                           "Ringing  SIP/2.0 200 OK":
                    ip_user = recived[recived.index("o=")
                                      + 2: recived.index("s=") - 2]
                    port_user_rtp \
                        = recived[recived.index("m=")
                                  + 2:recived.index("RTP")].split(" ")[1]

                    invite_user = [ip_user.split(" ")[1], port_user_rtp]
                    LINE = "ACK sip:" + sys.argv[3] + " SIP/2.0\r\n"
                    my_socket.send(bytes(LINE, 'utf-8'))
                    log_algorithm("Sent to " + proxy[0] + ":"
                                  + proxy[1] + ": " + LINE)
                    print("enviando:" + LINE[:LINE.index("\r\n")])
                    print("enviando RTP")
                    log_algorithm("Sent to " + invite_user[0] + ":"
                                  + invite_user[1] + ": "
                                  + audio_file + " via rtp")

                    csrc = []
                    for i in range(0, random.randint(3, 6)):
                        csrc.append(random.randint(1, 9999))
                    BIT = random.randint(0, 1)
                    RTP_header = simplertp.RtpHeader()
                    RTP_header.set_header(version=2, marker=BIT,
                                          payload_type=14,
                                          ssrc=200002, cc=len(csrc))
                    RTP_header.setCSRC(csrc)

                    audio = simplertp.RtpPayloadMp3(audio_file)
                    simplertp.\
                        send_rtp_packet(RTP_header, audio,
                                        invite_user[0], int(invite_user[1]))

        elif method == "REGISTER":
            try:
                option = int(option)
            except Exception:
                raise ValueError

            LINE = method + " sip:" + user_name + ":" + str(PORT)
            LINE = LINE + " SIP/2.0\r\nExpires:" + str(option) + "\r\n"
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) \
                    as my_socket:
                my_socket.setsockopt(socket.SOL_SOCKET,
                                     socket.SO_REUSEADDR, 1)
                my_socket.connect((proxy[0], int(proxy[1])))
                my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
                print("enviando:")
                print(LINE[:LINE.index("\r\n")])
                log_algorithm("Sent to " + proxy[0]
                              + ":" + proxy[1] + ": " + LINE)
                data = my_socket.recv(1024).decode('utf-8')
                print('Recibido -- ', data)
                log_algorithm("Received from "
                              + proxy[0] + ":" + proxy[1] + ": " + data)

                if data.split("\r\n")[0] == "SIP/2.0 401 Unauthorized":
                    nonce = data.split("nonce=")[1][1: -5]
                    h = hashlib.blake2b(key=nonce.encode(), digest_size=16)
                    h.update(password.encode())
                    LINE = LINE + "\r\n" + "Authorization: Digest response=\""
                    LINE = LINE + str(h.hexdigest()) + "\""
                    my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
                    print("enviando:")
                    print(LINE[:LINE.index("\r\n")])
                    log_algorithm("Sent to " + proxy[0]
                                  + ":" + proxy[1] + ": " + LINE)
                    data = my_socket.recv(1024).decode('utf-8')
                    print('Recibido -- ', data)
                    log_algorithm("Received from " + proxy[0]
                                  + ":" + proxy[1] + ": " + data)

        elif method == "BYE":

            LINE = method + " sip:" + sys.argv[3] \
                   + " SIP/2.0\r\nExpires:" + str(option) + "\r\n"
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM)\
                    as my_socket:
                my_socket.setsockopt(socket.SOL_SOCKET,
                                     socket.SO_REUSEADDR, 1)
                my_socket.connect((proxy[0], int(proxy[1])))
                print("enviando:")
                print(LINE[:LINE.index("\r\n")])
                log_algorithm("Sent to " + proxy[0] + ":" + proxy[1]
                              + ": " + LINE)
                my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
                data = my_socket.recv(1024)
                print('Recibido -- ', data.decode('utf-8'))
                log_algorithm("Received from " + proxy[0] + ":"
                              + proxy[1] + ": " + data.decode('utf-8'))

        else:
            LINE = method + "  " + user_name + "SIP/2.0\r\n"
            with socket.\
                    socket(socket.AF_INET, socket.SOCK_DGRAM)\
                    as my_socket:
                my_socket\
                    .setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                my_socket.connect((IP, PORT))
                my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
                data = my_socket.recv(1024)
                print('Recibido -- ', data.decode('utf-8'))
                log_algorithm("Received from " + proxy[0] + ":"
                              + proxy[1] + ": " + data.decode('utf-8'))

    except ValueError:
        print("Usage: python uaclient.py config method option")

    except ConnectionResetError:
        print("20101018160243 Error: No server listening at "
              + proxy[0] + " port " + str(proxy[1]))
        log_algorithm("Error: no se pudo conectar")


def log_algorithm(message):
    log_file = open(Log_path, "a")
    actual_time = time.strftime('%Y-%m-%d %H:%M:%S',
                                time.gmtime(time.time())).split(" ")
    actual_time[1] = actual_time[1].split(":")
    hora = int(actual_time[1][0]) * 3600
    mins = int(actual_time[1][1]) * 60
    seg = int(actual_time[1][2]) + hora + mins
    actual_time[1] = str(seg)
    log_info = ""
    for i in message:
        if i == "\r" or i == "\n":
            log_info = log_info + " "
        else:
            log_info = log_info + i
    log_file.write(actual_time[0] + " "
                   + actual_time[1] + "  " + log_info + "\r\n")
    log_file.close()


class XMLHandler(ContentHandler):

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """

        self.ua_ip = ""
        self.ua_puerto = ""
        self.proxy_ip = ""
        self.proxy_puerto = ""
        self.log_path = ""
        self.username = ""
        self.audio = ""
        self.rtp_puerto = ""
        self.passwd = ""

    def startElement(self, name, attrs):

        if name == 'uaserver':

            self.ua_ip = attrs.get("ip")
            self.ua_puerto = attrs.get("puerto")

        elif name == 'regproxy':
            self.proxy_ip = attrs.get("ip")
            self.proxy_puerto = attrs.get("puerto")

        elif name == 'log':
            self.log_path = attrs.get("path")

        elif name == "account":
            self.username = attrs.get("username")
            self.passwd = attrs.get("passwd")

        elif name == "audio":
            self.audio = attrs.get("path")

        elif name == "rtpaudio":
            self.rtp_puerto = attrs.get("puerto")


if __name__ == "__main__":

    FILE = sys.argv[1]
    parser = make_parser()
    cHandler = XMLHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(FILE))
    IP = str(cHandler.ua_ip)
    PORT = int(cHandler.ua_puerto)
    proxy = [cHandler.proxy_ip, cHandler.proxy_puerto]
    user_name = cHandler.username
    audio_file = cHandler.audio
    rtp_puerto = str(cHandler.rtp_puerto)
    direction = [IP, PORT]
    Log_path = cHandler.log_path
    password = cHandler.passwd
    log_algorithm("Starting client...")

    file = open(Log_path, "a")
    file.close()

    if direction[0] == "":
        IP = "127.0.0.1"
        direction[0] = "127.0.0.1"

    if proxy[0] == "":
        proxy[0] = "127.0.0.1"
    message_sender()
