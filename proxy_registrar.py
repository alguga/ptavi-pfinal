import socket
import socketserver
import sys
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import time
import json
import secrets
import hashlib

Client_Diccionary = {}
password_Diccionary = {}
expected_answer = ""


class MessageHandler(socketserver.DatagramRequestHandler):
    """ Gestiona la llegada de paquetes del destinatario"""

    def handle(self):

        linea = ""
        while 1:
            """ Bucle que lee los mensajes que recibe del cliente"""
            line = self.rfile.read()
            try:
                linea = linea + line.decode('utf-8')
            except ValueError:
                linea = linea + ""

                # Si no hay más líneas salimos del bucle infinito
            if not line:
                break
        print("Se ha recibido: " + str(linea.split("\r\n")[0]))
        log_message = "Received from " + self.client_address[0] + ":" \
                      + str(self.client_address[1]) + ": " + linea
        log_algorithm(log_message)
        global con_port
        global con_IP
        global expected_answer
        message_type = linea[0: linea.index(" ")]
        expires_handler()

        if message_type == "REGISTER":
            client_name = linea[linea.index(":") + 1:linea.index(" SIP/2.0")]
            client_port = client_name.split(":")[1]
            client_name = client_name.split(":")[0]
            expires = linea.split("\r\n")[1].split(":")[1]
            auth = linea.split("\r\n")

            if len(auth) == 4 and (client_name in password_Diccionary):
                nonce = str(secrets.randbits(8))
                message = "SIP/2.0 401 Unauthorized\r\n" \
                          + "WWW Authenticate: Digest nonce=\"" \
                          + nonce + "\"\r\n\r\n"
                self.wfile.write(message.encode())

                print("Se ha enviado: " + str(message))
                log_message = "Sent to  " + self.client_address[0] + ":" + \
                              str(self.client_address[1]) + ": " + message
                log_algorithm(log_message)

                h = hashlib.blake2b(key=nonce.encode(), digest_size=16)
                password_client = password_Diccionary[client_name]
                h.update(password_client.encode())
                expected_answer = str(h.hexdigest())

            elif len(auth) == 5 and (client_name in password_Diccionary):

                response = linea[linea.index("response=") + 10:-3]
                if response == expected_answer:
                    actual_time = str(time.time())
                    Client_Diccionary[client_name] \
                        = [self.client_address[0],
                           client_port, actual_time,
                           expires]
                    with open(database_json, "w") as file_database:
                        json.dump(Client_Diccionary, file_database)
                    self.wfile.write(bytes("SIP/2.0 200 OK" + "\r\n",
                                           'utf-8'))

                print("Se ha enviado: " + "SIP/2.0 200 OK")
                log_message = "Sent to  " \
                              + self.client_address[0] \
                              + ":" + str(self.client_address[1]) \
                              + ": " + "SIP/2.0 200 OK"
                log_algorithm(log_message)

            else:
                log_message \
                    = "Sent to " + self.client_address[0] \
                      + ":" + str(self.client_address[1]) \
                      + ": " + "SIP/2.0 404 User Not Found"
                log_algorithm(log_message)
                print("Se ha enviado: SIP/2.0 404 User Not Found ")
                self.wfile.write(bytes("SIP/2.0 404 User Not Found"
                                       + "\r\n", 'utf-8'))

            expires_handler()

        elif message_type == "INVITE":
            """ se parte de la premisa de que el cliente 1
            siempre envia al cliente 2"""
            client1 = linea[linea.index(":") + 1: linea.index(" SIP/2.0")]
            client2 = linea[linea.index("o=")
                            + 2: linea.index("s=") - 2].split(" ")[0]

            if client1 in Client_Diccionary and client2 in Client_Diccionary:
                ip_client = Client_Diccionary[client1][0]
                port_client = Client_Diccionary[client1][1]

                print(ip_client + " " + port_client)

                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) \
                        as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    my_socket.connect((ip_client, int(port_client)))
                    LINE = linea

                    my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
                    log_message = "Sent to " + ip_client + ":" \
                                  + str(port_client) + ": " + LINE
                    log_algorithm(log_message)
                    print("enviando: " + LINE[:LINE.index("\r\n")])
                    data = my_socket.recv(1024)

                    recived = data.decode('utf-8')
                    print('Recibido -- ', recived[: recived.index("\r\n")])
                    log_message = "Received from " + ip_client \
                                  + ":" + str(port_client) + ": "\
                                  + recived
                    log_algorithm(log_message)

                    self.wfile.write(str.encode(recived))
                    print("enviando: " + recived[: recived.index("\r\n")])
                    log_message \
                        = "Sent to " + Client_Diccionary[client2][0] \
                          + ":" + str(Client_Diccionary[client2][1]) \
                          + ": " + recived
                    log_algorithm(log_message)

            else:
                log_message \
                    = "Sent to "\
                      + Client_Diccionary[client2][0] \
                      + ":" + str(Client_Diccionary[client2][1]) \
                      + ": " + "SIP/2.0 404 User Not Found"
                log_algorithm(log_message)
                print("Se ha enviado: SIP/2.0 404 User Not Found ")
                self.wfile.write(b"SIP/2.0 404 User Not Found\r\n")

        elif message_type == "ACK":

            client1 = linea[linea.index("sip:") + 4: linea.index(" SIP/2.0")]
            ip_client = Client_Diccionary[client1][0]
            port_client = Client_Diccionary[client1][1]

            if client1 in Client_Diccionary:

                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) \
                        as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    my_socket.connect((ip_client, int(port_client)))
                    my_socket.send(bytes(linea, 'utf-8') + b'\r\n')
                    print("enviando: " + linea[:linea.index("\r\n")])
                    log_message = "Sent to " + ip_client + ":" \
                                  + str(port_client) + ": " + linea
                    log_algorithm(log_message)
            else:

                log_message = "Sent to " + self.client_address[0] + ":" \
                              + str(self.client_address[1]) + ": " + linea
                log_algorithm(log_message)
                print("Se ha enviado: SIP/2.0 404 User Not Found ")
                self.wfile.write(b"SIP/2.0 404 User Not Found\r\n")

        elif message_type == "BYE":

            client1 = linea[linea.index("sip:") + 4: linea.index(" SIP/2.0")]
            ip_client = Client_Diccionary[client1][0]
            port_client = Client_Diccionary[client1][1]

            if client1 in Client_Diccionary:
                with socket.socket(socket.AF_INET,
                                   socket.SOCK_DGRAM) as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    my_socket.connect((ip_client,
                                       int(port_client)))
                    my_socket.send(bytes(linea, 'utf-8') + b'\r\n')
                    print("enviando: " + linea)
                    log_message = "Sent to " + ip_client \
                                  + ":" + str(port_client) + ": " + linea
                    log_algorithm(log_message)
                    data = my_socket.recv(1024)
                    recived = data.decode('utf-8')
                    log_message = "Recived from " + ip_client + ":" \
                                  + str(port_client) + ": " + recived
                    log_algorithm(log_message)
                    print('Recibido -- ', recived[:recived.index("\r\n")])

                    self.wfile.write(str.encode(recived))
                    print("enviando: " + recived[: recived.index("\r\n")])
                    # comprobar que la ip a la que envia esta correcta
                    log_message = "Sent to " \
                                  + self.client_address[0] + ":" \
                                  + str(self.client_address[1]) + ": "\
                                  + recived
                    log_algorithm(log_message)

            else:
                log_message = "Sent to " + self.client_address[0] + ":" \
                              + str(self.client_address[1]) + ": " + linea
                log_algorithm(log_message)
                print("Se ha enviado: SIP/2.0 404 User Not Found ")
                self.wfile.write(b"SIP/2.0 404 User Not Found\r\n")


class XMLHandler(ContentHandler):
    """
    Clase para manejar chistes malos
    """

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """
        self.name = ""
        self.ip = ""
        self.puerto = ""
        self.path = ""
        self.passwdpath = ""
        self.log_in = False
        self.log = ""

    def startElement(self, name, attrs):

        if name == 'server':
            self.name = attrs.get("name")
            self.ip = attrs.get("ip")
            self.puerto = attrs.get("puerto")

        elif name == 'database':
            self.path = attrs.get("path")
            self.passwdpath = attrs.get("passwdpath")

        elif name == 'log':
            self.log_in = True

    def characters(self, char):
        if self.log_in:
            self.log = char
            self.log_in = False


def log_algorithm(message):
    log_file = open(Log_path, "a")
    actual_time = time.strftime('%Y-%m-%d %H:%M:%S',
                                time.gmtime(time.time())).split(" ")
    actual_time[1] = actual_time[1].split(":")
    hora = int(actual_time[1][0]) * 3600
    mins = int(actual_time[1][1]) * 60
    seg = int(actual_time[1][2]) + hora + mins
    actual_time[1] = str(seg)

    log_info = ""
    for i in message:
        if i == "\r" or i == "\n":
            log_info = log_info + " "
        else:
            log_info = log_info + i
    log_file.write(actual_time[0] + " " + actual_time[1]
                   + " " + log_info + "\r\n")
    log_file.close()


def expires_handler():
    actual_time = int(time.time())
    for i in Client_Diccionary.copy().keys():
        expired_date = int(float(Client_Diccionary[i][2])) \
                       + int(Client_Diccionary[i][3])
        if expired_date <= actual_time:
            del Client_Diccionary[i]

    with open(database_json, "w") as file_database:
        json.dump(Client_Diccionary, file_database)


if __name__ == "__main__":

    try:

        FILE = sys.argv[1]
        parser = make_parser()
        cHandler = XMLHandler()
        parser.setContentHandler(cHandler)
        parser.parse(open(FILE))
        IP = str(cHandler.ip)
        if IP == "":
            IP = "127.0.0.1"
        PORT = int(cHandler.puerto)
        Password_Path = cHandler.passwdpath
        Log_path = cHandler.log
        proxy_name = cHandler.name
        database_json = "register.json"

        try:
            file = open(database_json, "r")
            Client_Diccionary = json.load(file)
            file.close()
        except Exception:
            file = open(database_json, "w")
            file.close()

        try:
            file = open(Password_Path, "r")
            password_Diccionary = json.load(file)
            file.close()
        except Exception:
            file = open(Password_Path, "a")
            password_Diccionary = {}
            file.close()

        log_algorithm("Starting...")

        serv = socketserver.UDPServer((IP, PORT), MessageHandler)
        print("Server " + proxy_name + " listening at port "
              + str(PORT) + "...")
        serv.serve_forever()

    except ValueError:
        print("Usage: python proxy_registrar.py config")

    except IndexError:
        print("Usage: python proxy_registrar.py config")
